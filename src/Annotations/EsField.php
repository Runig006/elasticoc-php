<?php

namespace ElasticSearchOC\Annotations;

use Doctrine\Common\Annotations\Annotation;
use Exception;


/**
 * @Annotation
 * @Target({"PROPERTY","ANNOTATION"})
 */
abstract class EsField
{
    /** @Required */
    protected $propertyName;
    protected $canNull = TRUE;
    protected $canArray = TRUE;
    protected $default = NULL;

    public function __construct($options)
    {
        if (isset($options['value'])) {
            $options['propertyName'] = $options['value'];
            unset($options['value']);
        }

        foreach ($options as $key => $value) {
            if (!property_exists($this, $key)) {
                throw new \InvalidArgumentException(sprintf('Property "%s" does not exist', $key));
            }
            $this->$key = $value;
        }
        if ($this->propertyName == null) {
            throw new Exception("Property Name Not Set");
        }
    }

    public function getPropertyName()
    {
        return $this->propertyName;
    }

    public function getDefault()
    {
        return $this->default;
    }

    protected function returnIfValid($value)
    {
        if (is_array($value) && !$this->canArray) {
            throw new Exception($this->propertyName . " can be array");
        }

        if (count($value) == 0) {
            $value = NULL;
        } else if (count($value) == 1 && $this->isAssoc($value) == false) {
            $value = reset($value);
        }
        if ($value === NULL) {
            $value = $this->default;
        }
        if (!$this->canNull && $value === NULL) {
            throw new Exception($this->propertyName . " cannot be null");
        }
        return $value;
    }

    protected function isAssoc(array $arr)
    {
        if (array() === $arr) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    abstract protected function transformToSave($value);
    abstract protected function transformToLoad($value);
}
