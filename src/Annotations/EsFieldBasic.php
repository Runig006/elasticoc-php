<?php

namespace ElasticSearchOC\Annotations;

use Doctrine\Common\Annotations\Annotation;
use Exception;

/**
 * @Annotation
 * @Target({"PROPERTY","ANNOTATION"})
 */
class EsFieldBasic extends EsField
{
    const STRINGTYPE = "string";
    const INTEGERTYPE = "integer";
    const FLOATTYPE = "float";
    const NODECIMALTYPE = "nodecimal";
    const BOOLTYPE = "bool";

    /**
     * @Enum({"string", "integer", "float", "bool", "nodecimal"})
     */
    protected $dataType = 'string';
    protected $decimals = NULL;

    //SAVE
    public function transformToSave($value)
    {
        if (!is_array($value)) {
            $value = [$value];
        }

        //TRANSFORM THE VALUES
        foreach ($value as $key => $v) {
            if (is_array($v)) {
                $v = $this->transformToSave($v);
            } else {
                list($toConvert, $v) = $this->transformValueToSave($v);
                if ($toConvert !== NULL && $v !== NULL) {
                    settype($v, $toConvert);
                }
            }
            $value[$key] = $v;
        }

        //CHECK IF IT CAN BE NULL, OR HAS A DEFAULT
        return $this->returnIfValid($value);
    }

    /**
     * Transform, if needed it, the value. NOT THE TIPE
     *
     * @param [any] $value
     * @return [$toConvert, $value]
     */
    private function transformValueToSave($value)
    {
        $toConvert = $this->dataType;
        switch ($this->dataType) {
            case self::BOOLTYPE:
                $value = filter_var($value, FILTER_VALIDATE_BOOLEAN);
                break;
            case self::NODECIMALTYPE:
                if ($this->decimals > 0) {
                    $value = round($value * (pow(10, $this->decimals)), 0);
                } else {
                    throw new Exception("Decimals not set");
                }
                $toConvert = self::INTEGERTYPE;
                break;
            case self::FLOATTYPE:
                if ($this->decimals > 0) {
                    $value = round($value, $this->decimals);
                } else {
                    throw new Exception("Decimals not set");
                }
                break;
        }
        return [$toConvert, $value];
    }


    //LOAD
    public function transformToLoad($value)
    {
        if (!is_array($value)) {
            $value = [$value];
        }

        //TRANSFORM THE VALUES
        foreach ($value as $key => $v) {
            if (is_array($v)) {
                $v = $this->transformToLoad($v);
            } else {
                list($toConvert, $v) = $this->transformValueToLoad($v);
                if ($toConvert !== NULL && $v !== NULL) {
                    settype($v, $toConvert);
                }
            }
            $value[$key] = $v;
        }

        //CHECK IF IT CAN BE NULL, OR HAS A DEFAULT
        return $this->returnIfValid($value);
    }

    private function transformValueToLoad($value)
    {
        $toConvert = $this->dataType;
        switch ($this->dataType) {
            case self::BOOLTYPE:
                $value = filter_var($value, FILTER_VALIDATE_BOOLEAN);
                break;
            case self::NODECIMALTYPE:
                if ($this->decimals > 0) {
                    $value = round($value / (pow(10, $this->decimals)), $this->decimals);
                } else {
                    throw new Exception("Decimals not set");
                }
                $toConvert = self::FLOATTYPE;
                break;
            case self::FLOATTYPE:
                if ($this->decimals > 0) {
                    $value = round($value, $this->decimals);
                } else {
                    throw new Exception("Decimals not set");
                }
                break;
        }
        return [$toConvert, $value];
    }
}
