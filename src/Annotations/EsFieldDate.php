<?php

namespace ElasticSearchOC\Annotations;

use Doctrine\Common\Annotations\Annotation;
use Exception;
use DateTimeInterface;
use DateTime;


/**
 * @Annotation
 * @Target({"PROPERTY","ANNOTATION"})
 */
class EsFieldDate extends EsField
{
    const DEFAULT_FORMAT = "d-m-Y H:i:s";
    const DATE_FORMAT = "d-m-Y";
    const INDEX_FORMAT = "Ym";

    /** @Required */
    protected $saveFormat =  DateTimeInterface::ATOM;

    public function transformToSave($value)
    {
        if (!is_array($value)) {
            $value = [$value];
        }

        //TRANSFORM THE VALUES
        foreach ($value as $key => $v) {
            if (is_array($v)) {
                $v = $this->transformToSave($v);
            } else {
                $v = $this->transformValueToSave($v);
            }
            $value[$key] = $v;
        }

        //CHECK IF IT CAN BE NULL, OR HAS A DEFAULT
        return $this->returnIfValid($value);
    }

    /**
     * Transform, if needed it, the value. NOT THE TIPE
     *
     * @param [any] $value
     * @return [$toConvert, $value]
     */
    private function transformValueToSave($value)
    {
        if (!is_a($value, 'DateTime')) {
            $value = new DateTime($value);
        }
        $value = $value->format($this->saveFormat);
        return $value;
    }

    //LOAD
    public function transformToLoad($value)
    {
        if (!is_array($value)) {
            $value = [$value];
        }

        //TRANSFORM THE VALUES
        foreach ($value as $key => $v) {
            if (is_array($v)) {
                $v = $this->transformToLoad($v);
            } else {
                $v = $this->transformValueToLoad($v);
                $value[$key] = $v;
            }
            $value[$key] = $v;
        }

        //CHECK IF IT CAN BE NULL, OR HAS A DEFAULT
        return $this->returnIfValid($value);
    }

    private function transformValueToLoad($value)
    {
        $value = new DateTime($value);
        if (!is_a($value, 'DateTime')) {
            throw new Exception($value . " is not a DateTime");
        }
        return $value;
    }
}
