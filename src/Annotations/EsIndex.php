<?php

namespace ElasticSearchOC\Annotations;

use Doctrine\Common\Annotations\Annotation;
use Exception;

/**
 * @Annotation
 * @Target("CLASS")
 */
class EsIndex
{
    const DEFAULT_URL = 'localhost';
    const DEFAULT_PORT = '9200';
    const DEFAULT_PROTOCOL = 'http';
    const DEFAULT_USER = NULL;
    const DEFAULT_PASS = NULL;

    const ENV_PROTOCOL = 'ES_PROTOCOL';
    const ENV_URL = 'ES_URL';
    const ENV_PORT = 'ES_PORT';
    const ENV_USER = 'ES_USER';
    const ENV_PASSWORD = 'ES_PASSWORD';

    /** @Required */
    private $index;
    private $templateField;
    private $timeFormat = EsFieldDate::INDEX_FORMAT;

    private $url = SELF::ENV_URL;
    private $port = SELF::ENV_PORT;
    private $protocol = SELF::ENV_PROTOCOL;
    private $user = SELF::ENV_USER;
    private $pass = SELF::ENV_PASSWORD;

    private $retry = 2;
    private $sslVerification = true;
    private $disableTimestamps = false;

    public function __construct($options)
    {
        if (isset($options['value'])) {
            $options['index'] = $options['value'];
            unset($options['value']);
        }

        foreach ($options as $key => $value) {
            if (!property_exists($this, $key)) {
                throw new \InvalidArgumentException(sprintf('Property "%s" does not exist', $key));
            }
            $this->$key = $value;
        }
    }

    public function getTemplateFieldAndTimeFormat()
    {
        return [$this->templateField, $this->timeFormat];
    }

    public function mountIndex($templateValue = null)
    {
        $index = $this->index;
        if ($templateValue) {
            $index = str_replace("?", $templateValue, $this->index);
        }
        return $index;
    }

    public function mountHost()
    {
        if (getenv($this->url)) {
            $url = getenv($this->url);
        } else if ($this->url && $this->url != self::ENV_URL) {
            $url = $this->url;
        } else {
            $url = self::DEFAULT_URL;
        }

        if (getenv($this->port)) {
            $port = getenv($this->port);
        } else if ($this->port && $this->port != self::ENV_PORT) {
            $port = $this->port;
        } else {
            $port = self::DEFAULT_PORT;
        }

        if (getenv($this->protocol)) {
            $protocol = getenv($this->protocol);
        } else if ($this->protocol && $this->protocol != self::ENV_PROTOCOL) {
            $protocol = $this->protocol;
        } else {
            $protocol = self::DEFAULT_PROTOCOL;
        }

        if (getenv($this->user)) {
            $user = getenv($this->user);
        } else if ($this->user && $this->user != self::ENV_USER) {
            $user = $this->user;
        } else {
            $user = self::DEFAULT_USER;
        }

        if (getenv($this->pass)) {
            $pass = getenv($this->pass);
        } else if ($this->pass && $this->pass != self::ENV_PASSWORD) {
            $pass = $this->pass;
        } else {
            $pass = self::DEFAULT_PASS;
        }

        $hosts = [
            'host' => $url,
            'port' => $port,
            'scheme' => $protocol,
            'user' => $user,
            'pass' => $pass,
        ];

        $options = [
            'retry' => $this->retry,
            'sslVerification' => $this->sslVerification,
            'disableTimestamps' => $this->disableTimestamps,
        ];

        return [$hosts, $options];
    }
}
