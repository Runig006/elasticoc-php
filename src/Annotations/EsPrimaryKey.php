<?php

namespace ElasticSearchOC\Annotations;

use Doctrine\Common\Annotations\Annotation;
use Exception;


/**
 * @Annotation
 * @Target({"CLASS"})
 */
class EsPrimaryKey
{
    private $properties = null;
    private $md5 = false;

    public function __construct($options)
    {
        if (isset($options['value'])) {
            $options['properties'] = $options['value'];
            unset($options['value']);
        }

        foreach ($options as $key => $value) {
            if (!property_exists($this, $key)) {
                throw new \InvalidArgumentException(sprintf('Property "%s" does not exist', $key));
            }
            $this->$key = $value;
        }

        if (empty($this->properties)) {
            throw new Exception("Property Name can't be null");
        }
        if(!is_array($this->properties)){
            $this->properties = [$this->properties];
        }
    }

    public function getProperties()
    {
        return $this->properties;
    }
    public function getMd5()
    {
        return $this->md5;
    }
}
