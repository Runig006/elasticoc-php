<?php

namespace ElasticSearchOC\Engine\AnnotationsEngine;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\FileCacheReader;

use ElasticSearchOC\Engine\AnnotationsEngine\AnnotationsIndex;
use ElasticSearchOC\Engine\AnnotationsEngine\AnnotationsPrimaryKeys;
use ElasticSearchOC\Engine\AnnotationsEngine\AnnotationsField;

use ElasticSearchOC\Utils\ClassUtils;
use Exception;

class AnnotationsConvert
{
    use ClassUtils;

    const GETSTRING = "get";
    const SETSTRING = "set";

    const FIELD_ANNOTATION_CLASS = 'ElasticSearchOC\Annotations\EsFieldBasic';
    const DATE_ANNOTATION_CLASS = 'ElasticSearchOC\Annotations\EsFieldDate';
    const INDEX_ANNOTATION_CLASS = 'ElasticSearchOC\Annotations\EsIndex';
    const FIELD_PRIMARY_ANNOTATION_CLASS = 'ElasticSearchOC\Annotations\EsPrimaryKey';

    function __construct()
    {
        $this->reader = new FileCacheReader(
            new AnnotationReader(),
            sys_get_temp_dir() . "/elasticOc/reader/cache",
            $debug = true
        );
        $check = class_exists(self::FIELD_ANNOTATION_CLASS, true);
        if (!$check) {
            throw new Exception("Can't load the field annotation");
        }
        $check = class_exists(self::DATE_ANNOTATION_CLASS, true);
        if (!$check) {
            throw new Exception("Can't load the date annotation");
        }
        $check = class_exists(self::INDEX_ANNOTATION_CLASS, true);
        if (!$check) {
            throw new Exception("Can't load the index annotation");
        }
        $check = class_exists(self::FIELD_PRIMARY_ANNOTATION_CLASS, true);
        if (!$check) {
            throw new Exception("Can't load the primary key annotation");
        }
    }

    //ES INDEX
    public function mountHostFromEntity($originalObject)
    {
        return AnnotationsIndex::mountHostFromEntity($originalObject, $this->reader);
    }

    public function mountIndexFromEntity($originalObject)
    {
        return AnnotationsIndex::mountIndexFromEntity($originalObject, $this->reader);
    }

    public function mountIndexFromArray($originalObject, $values)
    {
        return AnnotationsIndex::mountIndexFromArray($originalObject, $values, $this->reader);
    }

    //ES FIELDS PRIMARY
    public function generateId($originalObject)
    {
        return AnnotationsPrimaryKeys::generateId($originalObject, $this->reader);
    }

    //ES FIELDS
    public function convertObjectToArray($originalObject)
    {
        return AnnotationsField::convertObjectToArray($originalObject, $this->reader);
    }

    public function convertArrayToObject($hit, $object)
    {
        return AnnotationsField::convertArrayToObject($hit, $object, $this->reader);
    }
}
