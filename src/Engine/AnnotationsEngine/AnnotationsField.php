<?php

namespace ElasticSearchOC\Engine\AnnotationsEngine;

use ElasticSearchOC\Utils\ClassUtils;
use Exception;

class AnnotationsField
{

    //ES FIELDS
    static public function convertObjectToArray($originalObject, $reader)
    {
        $convertedObject = [];
        $reflectionObject = new \ReflectionObject($originalObject);
        foreach ($reflectionObject->getProperties() as $property) {
            $annotation = $reader->getPropertyAnnotations($property);
            if ($annotation) {
                if (count($annotation) > 1) {
                    throw new Exception("More than 1 Field Annotation");
                }
                $annotation = reset($annotation);
                list($key, $value) = AnnotationsUtils::convertPropertySaving($property, $originalObject, $annotation);
                $convertedObject[$key] = $value;
            }
        }
        return $convertedObject;
    }

    static public function convertArrayToObject($hit, $originalObject, $reader)
    {
        $reflectionObject = new \ReflectionObject($originalObject);
        foreach ($reflectionObject->getProperties() as $property) {
            $annotation = $reader->getPropertyAnnotations($property);
            if ($annotation) {
                if (count($annotation) > 1) {
                    throw new Exception("More than 1 Field Annotation");
                }
                $annotation = reset($annotation);
                if (isset($hit['_source'][$annotation->getPropertyName()])) {
                    $value = $hit['_source'][$annotation->getPropertyName()];
                } else {
                    $value = $annotation->getDefault();
                }
                $originalObject = AnnotationsUtils::convertPropertyLoading($value, $property, $originalObject, $annotation);
            }
        }
        return $originalObject;
    }
}
