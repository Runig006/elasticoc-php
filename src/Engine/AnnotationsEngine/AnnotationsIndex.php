<?php

namespace ElasticSearchOC\Engine\AnnotationsEngine;

use ElasticSearchOC\Utils\ClassUtils;
use ElasticSearchOC\Engine\AnnotationsEngine\AnnotationsUtils;
use DateTime;
use Exception;

class AnnotationsIndex
{
    /**
     * Mount the host config from the entity
     *
     * @param Object $originalObject
     * @param AnnotationReader $reader
     * @return Array
     */
    static public function mountHostFromEntity($originalObject, $reader)
    {
        $hosts = [];
        $options = [];
        $reflectionObject = new \ReflectionObject($originalObject);
        $annotation = $reader->getClassAnnotations($reflectionObject);
        if (!$annotation) {
            throw new Exception("This class doesn't have any Index Configurated");
        }
        $annotation = AnnotationsUtils::cleanAnnotations($annotation, AnnotationsConvert::INDEX_ANNOTATION_CLASS);
        foreach ($annotation as $a) {
            list($h, $o) = $a->mountHost();
            $hosts[] = $h;
            $options = array_merge($options, $o);
        }
        return [$hosts, $options];
    }

    /**
     * From one entity get the index that correspont to it
     *
     * @param Object $originalObject
     * @param AnnotationReader $reader
     * @return String
     */
    static public function mountIndexFromEntity($originalObject, $reader)
    {
        $index = [];
        $reflectionObject = new \ReflectionObject($originalObject);
        $annotation = $reader->getClassAnnotations($reflectionObject);
        $annotation = AnnotationsUtils::cleanAnnotations($annotation, AnnotationsConvert::INDEX_ANNOTATION_CLASS);
        foreach ($annotation as $a) {
            $value = NULL;
            list($templateField, $timeformat) = $a->getTemplateFieldAndTimeFormat();
            if ($templateField) {
                $value = static::customGetValueConverted($reflectionObject, $originalObject, $templateField, $timeformat, $reader);
            }
            $index[] = $a->mountIndex($value);
        }
        $index = implode(",", $index);
        return $index;
    }

    /**
     * Custom get, the only changes is the datetime, it will use the custom timeformat set in the annotation, the rest of the values will be
     * returned like if you are saving the value in the index
     *
     * @param Object $reflectionObject
     * @param Object $originalObject
     * @param string $templateField
     * @param string $timeformat
     * @param AnnotationReader $reader
     * @return Any
     */
    static public function customGetValueConverted($reflectionObject, $originalObject, $templateField, $timeformat, $reader)
    {
        list($annotationKey, $property) = AnnotationsUtils::getFieldAnnotationFromProperty($reflectionObject, $templateField, $reader);
        $propertyName = $property->name;
        if ($timeformat) {
            $value = AnnotationsUtils::getValueFromPropertyName($propertyName, $originalObject);
            $value = AnnotationsUtils::customGetTimeFormat($value, $timeformat);
        } else {
            list($property, $value) = AnnotationsUtils::convertPropertySaving($property, $originalObject, $annotationKey);
        }
        return $value;
    }


    /**
     * For an array of values mount all the index possible
     *
     * @param Object $originalObject
     * @param Any $values
     * @param AnnotationReader $reader
     * @return String
     */
    static public function mountIndexFromArray($originalObject, $values, $reader)
    {
        if (!is_array($values)) {
            $values = [
                0 => [
                    0 => $values
                ]
            ];
        } else if (!is_array(reset($values))) {
            $values = [$values];
        }

        $index = [];
        $reflectionObject = new \ReflectionObject($originalObject);
        $annotation = $reader->getClassAnnotations($reflectionObject);
        $annotation = AnnotationsUtils::cleanAnnotations($annotation, AnnotationsConvert::INDEX_ANNOTATION_CLASS);

        foreach ($annotation as $key => $a) {
            list($templateField, $timeformat) = $a->getTemplateFieldAndTimeFormat();
            if (!isset($values[$key])) {
                throw new Exception("The index in the annotation " . $key . " doesn't have a array of values");
            }
            $indexValues = $values[$key];
            foreach ($indexValues as $v) {

                if ($timeformat) {
                    $v = AnnotationsUtils::customGetTimeFormat($v, $timeformat);
                }
                $index[] = $a->mountIndex($v);
            }
        }
        $index = implode(",", $index);
        return $index;
    }
}
