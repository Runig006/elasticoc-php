<?php

namespace ElasticSearchOC\Engine\AnnotationsEngine;

use ElasticSearchOC\Engine\AnnotationsEngine\AnnotationsUtils;
use Exception;

class AnnotationsPrimaryKeys
{

    static public function generateId($originalObject, $reader)
    {
        $idValue = [];
        $reflectionObject = new \ReflectionObject($originalObject);
        $annotation = $reader->getClassAnnotations($reflectionObject);
        $annotation = AnnotationsUtils::cleanAnnotations($annotation, AnnotationsConvert::FIELD_PRIMARY_ANNOTATION_CLASS);
        if (count($annotation) > 1) {
            throw new Exception("More than 1 Primary Field Annotation");
        }
        if (count($annotation) == 1) {
            $annotation = reset($annotation);
            //GET THE PROPERTIES THAN FORM THE PRIMARY KEYS
            foreach ($annotation->getProperties() as $property) {
                $property = $reflectionObject->getProperty($property);
                
                //GET THE ANNOTATION OF A PROPERTY THAN IS A PRIMARY KEY
                $annotationKey = $reader->getPropertyAnnotations($property);
                if(count($annotationKey) > 1){
                    throw new Exception("More than 1 Field Annotation in a Primary Key");
                }
                $annotationKey = reset($annotationKey);

                //TRANSFORM THE VALUE OF THE PROPERTY
                list($property, $idValue[]) = AnnotationsUtils::convertPropertySaving($property, $originalObject, $annotationKey);
            }
            
            $idValue = implode("", $idValue);
            if ($annotation->getMd5()) {
                $idValue = md5($idValue);
            }
        } else {
            $idValue = uniqid("id", true);
        }

        if (empty($idValue)) {
            throw Exception("Null Id");
        }
        return $idValue;
    }

}
