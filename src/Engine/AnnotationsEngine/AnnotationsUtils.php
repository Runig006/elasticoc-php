<?php

namespace ElasticSearchOC\Engine\AnnotationsEngine;

use ElasticSearchOC\Annotations\EsFieldParent;
use Exception;
use DateTime;
class AnnotationsUtils
{
    /**
     * Get only the annotations of one Type
     *
     * @param Array $annotation
     * @param [String] $class
     * @return void
     */
    static public function cleanAnnotations($annotation, $class)
    {
        $annotation = array_filter($annotation, function ($a) use ($class) {
            return get_class($a) == $class;
        });
        return $annotation;
    }

    static public function getFieldAnnotationFromProperty($reflectionObject, $property, $reader)
    {
        if (!is_a($property, 'ReflectionProperty')) {
            $property = $reflectionObject->getProperty($property);
        }
        $annotation = $reader->getPropertyAnnotations($property);
        if (count($annotation) > 1) {
            throw new Exception("More than 1 Field Annotation in a Primary Key");
        }
        $annotation = reset($annotation);
        return [$annotation, $property];
    }

    /**
     * Given a property name and the object
     *
     * @param [String] $property
     * @param [Object] $originalObject
     * @param [Annotation] $annotation
     * @return void
     */
    static public function convertPropertySaving($property, $originalObject, $annotation)
    {
        $propertyName = $property->name;
        $value = self::getValueFromPropertyName($propertyName, $originalObject);
        $value = $annotation->transformToSave($value);
        $propertyName = $annotation->getPropertyName($annotation);
        return [$propertyName, $value];
    }

    /**
     * Get the value of a property given in string
     *
     * @param [String] $property
     * @param [Object] $originalObject
     * @return void
     */
    static public function getValueFromPropertyName($propertyName, $originalObject)
    {
        $function = AnnotationsConvert::GETSTRING . $propertyName;
        if (method_exists($originalObject, $function)) {
            $value = $originalObject->$function();
        } else if (property_exists($originalObject, $propertyName)) {
            $value = $originalObject->$propertyName;
        }
        return $value;
    }


    /**
     * Given a property name and the object
     *
     * @param [String] $property
     * @param [Object] $originalObject
     * @param [Annotation] $annotation
     * @return void
     */
    static public function convertPropertyLoading($value, $property, $originalObject, $annotation)
    {
        $propertyName = $property->name;
        $value = $annotation->transformToLoad($value);
        $originalObject = self::setValueFromPropertyName($value, $propertyName, $originalObject);
        return $originalObject;
    }

    /**
     * Set the value to a object property using the name of that property
     *
     * @param type $value
     * @param string $property
     * @param object $originalObject
     * @return object
     */
    static public function setValueFromPropertyName($value, $propertyName, $originalObject)
    {
        $function = AnnotationsConvert::SETSTRING . $propertyName;
        if (method_exists($originalObject, $function)) {
            $originalObject->$function($value);
        } else if (property_exists($originalObject, $propertyName)) {
            $originalObject->$propertyName = $value;
        }
        return $originalObject;
    }

    /**
     * Get the raw value of the property, transform it to a DateTime and then use the timeformat
     *
     * @param string $propertyName
     * @param object $originalObject
     * @param string $timeformat
     * @return void
     */
    static public function customGetTimeFormat($value, $timeformat)
    {
        if ($value) {
            if (!is_a($value, 'DateTime')) {
                $value = new DateTime($value);
            }
            $value = $value->format($timeformat);
        }
        return $value;
    }
}
