<?php

namespace ElasticSearchOC\Engine;

use ElasticSearchOC\Engine\AnnotationsEngine\AnnotationsConvert;
use ElasticSearchOC\Engine\ProxyClient;

class EsEntityManager
{
    protected static $proxy = [];
    protected static $converted = null;

    public static function getProxy($object)
    {
        $class = get_class($object);
        if (!isset(static::$proxy[$class])) {
            list($hosts, $options) = static::getConvert()->mountHostFromEntity($object);
            static::$proxy[$class] = new ProxyClient($hosts, $options);
        }
        return static::$proxy[$class];
    }

    public static function getConvert()
    {
        if (static::$converted == null) {
            static::$converted = new AnnotationsConvert();
        }
        return static::$converted;
    }
}
