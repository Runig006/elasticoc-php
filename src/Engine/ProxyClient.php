<?php

namespace ElasticSearchOC\Engine;

use ElasticSearchOC\Utils\ClassUtils;
use ElasticSearchOC\Entities\Bulk;
use Elasticsearch\ClientBuilder;

class ProxyClient
{
    use ClassUtils;
    const DEF_SCROLL = '30s';

    private $disableCreationTime = false;

    private function delete_custom_options($options)
    {
        unset($options['custom-index-values']);
        unset($options['custom-index']);
        unset($options['custom-index-values']);
        unset($options['force-update-create']);
        unset($options['doc_as_upsert']);
        return $options;
    }

    protected $elasticClient = null;
    protected $bulk = null;

    public function __construct($hosts, $options)
    {
        $this->elasticClient = ClientBuilder::create()
            ->setHosts($hosts)
            ->setRetries($options['retry'])
            ->setSSLVerification($options['sslVerification'])
            ->build();
        $this->disableTimestamps = $options['disableTimestamps'];
    }

    ////////////////////
    //
    // BULK
    //
    ////////////////////

    public function beginBulk()
    {
        $this->bulk = new Bulk();
    }

    public function commitBulk($options = [])
    {
        $options = $this->delete_custom_options($options);
        $params = array_merge($this->bulk->toArray(), $options);
        $res = $this->elasticClient->bulk($params);
        $this->bulk = null;
        return $res;
    }

    public function getBulk()
    {
        return $this->bulk;
    }

    ////////////////////
    //
    // TRANSLATE TO CLIENT
    //
    ////////////////////

    /**
     * Make a search
     *
     * @param Array $query
     * @param String $index
     * @param Array $options
     * @return Json
     */
    public function search($query, $index, $options = [])
    {
        $options = $this->delete_custom_options($options);

        $params = [
            'index' => $index,
            'body' => $query,
        ];
        $params = array_merge($params, $options);
        return  $this->elasticClient->search($params);
    }

    /**
     * Get the next page in a scroll
     *
     * @param String $scrollId
     * @param String $scrollTime
     * @param Array $options
     * @return Json
     */
    public function scroll($scrollId, $scrollTime = self::DEF_SCROLL, $options = [])
    {
        $options = $this->delete_custom_options($options);

        $params = [
            "scroll_id" => $scrollId,
            'scroll' => $scrollTime,
        ];
        return $this->elasticClient->scroll($params);
    }

    /**
     * Update the document, if is not created, but default, created it
     *
     * @param [Object] $item
     * @param Array $options
     * @return Array|Bulk
     */
    public function update($item, $id, $index, $options = [])
    {
        if ($this->bulk) {
            $this->bulk->update($item, $id, $index, $options);
            return $this->bulk;
        } else {
            $params = $this->mountUpdateParams($item, $id, $index, $options);
            $res = $this->elasticClient->update($params);
            return $res;
        }
    }

    /**
     * Prepare an update petition
     *
     * @param Array $item
     * @param String $id
     * @param String $index
     * @param Array $options
     * @return Array
     */
    private function mountUpdateParams($item, $id, $index, $options = [])
    {
        if (isset($options['force-update-create']) == false || $options['force-update-create'] == false) {
            unset($item['_created_at']);
        }

        if ($this->disableTimestamps == false) {
            $item = array_merge($item, array('_update_at' => Time() * 1000));
        }

        $params = [
            'index' => isset($options['index']) ? $options['index'] : $index,
            'id' => $id,
            'body' => [
                'doc' => $item,
            ],
        ];

        if ($this->disableTimestamps == false && isset($options['doc_as_upsert']) && $options['doc_as_upsert']) {
            $upsert = isset($item['_created_at']) ? $item : array_merge($item, array('_created_at' => Time() * 1000));
            $params['body']['upsert'] = $upsert;
        }

        $options = $this->delete_custom_options($options);
        $params = array_merge($params, $options);
        return $params;
    }

    /**
     * Delete all the objects that match a query
     *
     * @param Array $query
     * @param Array $options
     * @return Voide
     */
    public function deleteByQuery($query, $index, $options = [])
    {
        $options = $this->delete_custom_options($options);

        $params = [
            'index' => isset($options['index']) ? $options['index'] : $index,
            'body' => $query,
        ];

        return $this->elasticClient->deleteByQuery($params);
    }

    /**
     * Refresh an index
     *
     * @param String $index
     * @return void
     */
    public function refresh($index)
    {
        return $this->elasticClient->indices()->refresh(['index' => $index]);
    }
}
