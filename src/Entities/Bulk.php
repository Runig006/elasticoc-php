<?php

namespace ElasticSearchOC\Entities;

class Bulk
{
    protected $retryConflict = 3;
    protected $array = [];

    public function __construct()
    {
        $this->array = ['body' => []];
    }

    public function update($item, $id, $index, $options, $disableTimestamps = false)
    {
        //Basic Part
        $this->array['body'][] = [
            'update' => [
                '_index' => $index,
                '_id' => $id,
                'retry_on_conflict' => $this->retryConflict
            ]
        ];

        //Item part
        $params = [];
        if (isset($options['force-update-create']) == false || $options['force-update-create'] == false) {
            unset($item['_created_at']);
        }
        if ($disableTimestamps == false) {
            $item = array_merge($item, array('_update_at' => Time() * 1000));
        }
        $params['doc'] = $item;
        if ($disableTimestamps == false && isset($options['doc_as_upsert']) && $options['doc_as_upsert']) {
            $upsert = isset($item['_created_at']) ? $item : array_merge($item, array('_created_at' => Time() * 1000));
            $params['upsert'] = $upsert;
        }

        $this->array['body'][] = $params;
    }

    public function toArray()
    {
        return $this->array;
    }
}
