<?php

namespace ElasticSearchOC\Entities;

use ElasticSearchOC\Utils\ClassUtils;
use ElasticSearchOC\Utils\QueryUtils;
use ElasticSearchOC\Engine\EsEntityManager;
use Exception;

abstract class EsEntityV2
{
    use ClassUtils;
    protected $_uid = null;
    protected $_index = null;
    protected $_score = null;
    protected $_sort = null;

    public function ToJson()
    {
        return json_encode(ClassUtils::ToArray($this));
    }

    public function ToArray()
    {
        return ClassUtils::ToArray($this);
    }

    ////////
    // INDEX
    ////////

    /**
     * Get the index/s of this object, if is not set, generate it
     *
     * @return string
     */
    public function getIndex()
    {
        if (!$this->_index) {
            $this->_index = EsEntityManager::getConvert()->mountIndexFromEntity($this);
        }
        return $this->_index;
    }

    /**
     * Set the Index
     *
     * @param [string] $uid
     * @return void
     */
    public function setIndex($index)
    {
        $this->_index = $index;
    }

    /**
     * Get the index that will use for a call
     *
     * @param array $options
     * @return string
     */
    public function getQueryIndex(&$options)
    {
        if (isset($options['custom-index'])) {
            $index = $options['custom-index'];
            return $index;
        } else if (isset($options['custom-index-values'])) {
            $index = EsEntityManager::getConvert()->mountIndexFromArray($this, $options['custom-index-values']);
            return $index;
        } else {
            return EsEntityManager::getConvert()->mountIndexFromEntity($this);
        }
    }

    ////////
    // UID
    ////////

    /**
     * Get the unique id of this object, if is not set, generate it
     *
     * @return string
     */
    public function getUId()
    {
        if (!$this->_uid) {
            $this->_uid = EsEntityManager::getConvert()->generateId($this);
        }
        return $this->_uid;
    }

    /**
     * Set the UID
     *
     * @param [string] $uid
     * @return void
     */
    public function setUid($uid)
    {
        $this->_uid = $uid;
    }

    ////////
    // SCORE
    ////////

    /**
     * Get the unique id of this object, if is not set, generate it
     *
     * @return string
     */
    public function getScore()
    {
        return $this->_score;
    }

    /**
     * Set the Sort
     *
     * @param [string] $sort
     * @return void
     */
    public function setScore($score)
    {
        $this->_score = $score;
    }

    ////////
    // SORT
    ////////

    /**
     * Get the unique id of this object, if is not set, generate it
     *
     * @return string
     */
    public function getSort()
    {
        return $this->_sort;
    }

    /**
     * Set the Sort
     *
     * @param [string] $sort
     * @return void
     */
    public function setSort($sort)
    {
        $this->_sort = $sort;
    }

    ////////
    // BULKS
    ////////

    protected function beginBulk()
    {
        EsEntityManager::getProxy($this)->beginBulk();
    }
    protected function commitBulk()
    {
        return EsEntityManager::getProxy($this)->commitBulk();
    }

    ////////
    // GET
    ////////

    /**
     * Get all the objects that pass the filter (Only the objects)
     *
     * @param array $values
     * @param array $options
     * @return void
     */
    protected function get($values = [], $options = ['scroll' => QueryUtils::DEFSCROLL])
    {
        $query = QueryUtils::getDefaultQuery($values, QueryUtils::QUERYSCROLL);

        $res = $this->search($query, $options);

        $objects = $res->getObjects();
        $scroll = $res->getScrollIfNeed();

        while ($scroll) {
            $res = $this->scroll($scroll, $query);
            $objects = array_merge($objects, $res->getObjects());
        }

        return $objects;
    }

    /**
     * Get all the objects that pass the filter (Only the objects), if there is more or less than one, throw an Exception
     *
     * @param array $values
     * @param array $options
     * @return void
     */
    protected function getOne($values, $options = [])
    {
        $query = QueryUtils::getDefaultQuery($values, QueryUtils::QUERYSCROLL);
        $res = $this->search($query, $options);
        $objects = $res->getObjects();
        if (count($objects) == 1) {
            return reset($objects);
        } else {
            throw new Exception(count($objects) . ' number of items found');
        }
    }

    /**
     * Return a Elasticsearch Response paginated
     *
     * @param array $values
     * @param integer $size
     * @param [array] $search_after
     * @param array $options
     * @return void
     */
    protected function paginate($values = [], $size = 10, $search_after = null, $options = [])
    {
        $query = QueryUtils::getDefaultQuery($values, $size);
        if ($search_after) {
            $query['search_after'] = $search_after;
        }
        return $this->search($query, $options);
    }

    /**
     * Search a query
     *
     * @param array $query
     * @param array $options
     * @return EsResponse
     */
    protected function search($query = [], $options = [])
    {
        $multipleIndex = isset($options['custom-index-values']);
        $index = $this->getQueryIndex($options);
        if ((!isset($options['force-exists']) || $options['force-exists'] == FALSE) && isset($options['custom-index-values'])) {
            $index = str_replace(',', '*,', $index) . "*";
        }
        $index = str_replace('?', '*', $index);
        $res = EsEntityManager::getProxy($this)->search($query, $index, $options);
        return new EsResponse($res, $query, $this);
    }


    /**
     * Get the next result of a scroll
     *
     * @param String $scroll ScrollId
     * @param Array $query The original query of the scroll
     * @return EsResponse
     */
    protected function scroll($scroll, $query = null)
    {
        $res = EsEntityManager::getProxy($this)->scroll($scroll);
        return new EsResponse($res, $query, $this);
    }

    ////////
    // EDIT
    ////////

    /**
     * Call and update but with doc_as_upsert
     *
     * @param array $options
     * @return ElasticSearchResponse
     */
    public function save(array $options = array())
    {
        $options['doc_as_upsert'] = true;
        return $this->update($options);
    }

    /**
     * Update a document
     *
     * @param array $options
     * @return ElasticSearchResponse
     */
    public function update(array $options = array())
    {
        $id = $this->getUId();
        $index = $this->getQueryIndex($options);
        $object = EsEntityManager::getConvert()->convertObjectToArray($this);

        return EsEntityManager::getProxy($this)->update($object, $id, $index, $options);
    }

    ////////
    // DELETE
    ////////

    public function deleteThis(array $options = array())
    {
        $id = $this->getUId();
        return $this->delete([$id], $options);
    }

    /**
     * Delete using a filter
     *
     * @param Array $options
     * @return void
     */
    protected function delete($values = [], array $options = array())
    {
        $query = QueryUtils::getDefaultQuery($values, QueryUtils::QUERYSCROLL);
        unset($query['size']);
        unset($query['order']);
        return $this->deleteByQuery($query, $options);
    }

    /**
     * Delete all the documents that match the query
     *
     * @param Array $query
     * @param Array $options
     * @return void
     */
    protected function deleteByQuery($query, $options)
    {
        $index = $this->getQueryIndex($options);
        if ((!isset($options['force-exists']) || $options['force-exists'] == FALSE) && isset($options['custom-index-values'])) {
            $index = str_replace(',', '*,', $index) . "*";
        }
        $index = str_replace('?', '*', $index);
        EsEntityManager::getProxy($this)->deleteByQuery($query, $index, $options);
    }

    ////////
    // OTHERS
    ////////

    /**
     * Refresh a index
     *
     * @param Array $options
     * @return void
     */
    protected function refresh($options = [])
    {
        $index = $this->getQueryIndex($options);
        if ((!isset($options['force-exists']) || $options['force-exists'] == FALSE) && isset($options['custom-index-values'])) {
            $index = str_replace(',', '*,', $index) . "*";
        }
        $index = str_replace('?', '*', $index);
        $test = EsEntityManager::getProxy($this)->refresh($index);
    }
}
