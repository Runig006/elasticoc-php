<?php

namespace ElasticSearchOC\Entities;

use ElasticSearchOC\Engine\EsEntityManager;
use ElasticSearchOC\Utils\QueryUtils;

class EsResponse
{
    protected $objectClass = null;
    protected $originalRes = null;
    protected $query = null;

    protected $objects = [];
    protected $total = 0;
    protected $maxScore = 0;
    protected $aggs = [];

    protected $totalRelation = QueryUtils::EQREALTION;
    protected $scrollId = null;
    protected $searchAfter = null;

    public function __construct($res, $query, $objectClass)
    {
        $this->objectClass = $objectClass;
        $this->originalRes = $res;
        $this->query = $query;

        $this->objects = $this->mountObjects();
        if (isset($res['_scroll_id'])) {
            $this->scrollId = $res['_scroll_id'];
        }
        if (isset($res['hits']['total']['value'])) {
            $this->total = $res['hits']['total']['value'];
            $this->totalRelation = $res['hits']['total']['relation'];
        } else {
            $this->total = $res['hits']['total'];
        }
        if (isset($res['aggregations'])) {
            $this->aggs = $res['aggregations'];
        }
        if (isset($res['hits']['max_score'])) {
            $this->maxScore = $res['hits']['max_score'];
        }
    }

    /**
     * Get only the object with certain ID
     *
     * @param [type] $id
     * @return void|Object
     */
    public function getObject($id)
    {
        if (isset($this->objets[$id])) {
            return $this->objects[$id];
        } else {
            return NULL;
        }
    }

    /**
     * Return the array of all the objects
     *
     * @return void|array
     */
    public function getObjects()
    {
        return $this->objects;
    }

    /**
     * Return the last sort, the "search after"
     * @return array
     */
    public function getSearch_After()
    {
        if (count($this->objects) > 0) {
            $object = reset($this->objects);
            return $object->getSort();
        } else {
            return NULL;
        }
    }

    /**
     * Get the original res
     *
     * @return Json
     */
    public function getOriginalRes()
    {
        return $this->originalRes;
    }

    /**
     * Return total
     *
     * @return Integer
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Return get the max score
     *
     * @return Float
     */
    public function getMaxScore()
    {
        return $this->maxScore;
    }

    /**
     * Return the aggregations
     *
     * @return Array
     */
    public function getAggs()
    {
        return $this->aggs;
    }

    /**
     * If is needed return the Scroll Id
     *
     * @return void|string
     */
    public function getScrollIfNeed()
    {
        if ($this->objects && $this->scrollId && ($this->totalRelation != QueryUtils::EQREALTION || count($this->objects) < $this->total)) {
            return $this->scrollId;
        }
        return null;
    }

    ////////////
    /// MOUNTS
    ////////////

    /**
     * From the res mount all the objects
     *
     * @return Array
     */
    private function mountObjects()
    {
        $objects = [];
        foreach ($this->originalRes['hits']['hits'] as $hit) {
            $object = $this->mountObject($hit);
            $objects[$object->getUId()] = $object;
        }
        return $objects;
    }

    /**
     * From a hit of the response of a Elastic
     *
     * @param array $hit
     * @return Object
     */
    private function mountObject($hit)
    {
        $object = EsEntityManager::getConvert()->convertArrayToObject($hit, clone $this->objectClass);
        $object->setUid($hit['_id']);
        $object->setIndex($hit['_index']);
        $object->setScore($hit['_score']);
        if (isset($hit['sort'])) {
            $object->setSort($hit['sort']);
        }
        return $object;
    }
}
