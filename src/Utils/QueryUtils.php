<?php

namespace ElasticSearchOC\Utils;

use PHPUnit\Runner\Exception;

class QueryUtils
{
    const AGGSFULL = 2147483647;

    const DEFSCROLL = "5s";
    const QUERYFULL = 10000;
    const QUERYSCROLL = 1000;
    const QUERYSIZEDEFAULT = 10;

    const MAXTERMS = 4096;
    const FULLPRECISION = 40000;

    const ORDNEGATIVE = "desc";
    const ORDPOSITIVE = "asc";

    const EQREALTION = "eq";
    const GTERELATION = "gte";

    /**
     * 
     * @param type $field
     * @param type $values
     * @param type $size
     * @return type
     */
    public static function getDefaultQuery($values = null, $size = null)
    {
        $query = [
            "size" => $size == null ? self::QUERYFULL : $size,
            'sort' => [
                '_id' => 'desc'
            ]
        ];
        if ($values != null) {
            $values = self::mountValues($values);
            $filter = self::mountTermsPart($values);
            if (count($filter) > 0) {
                $query['query']['bool']['filter'] = $filter;
            }
        }
        return $query;
    }

    /**
     * The filters can be a string (For asking for the _id), and array...for the id. You know that type of things
     *
     * @param [string|array] $values
     * @return void
     */
    private static function mountValues($values)
    {
        if (!is_array($values)) {
            $values = ["_id" => [$values]];
        } else if (!is_array(reset($values))) {
            $values = ["_id" => $values];
        }
        return $values;
    }


    //////////////////
    //
    // MOUNT QUERY PARTS
    //
    //////////////////

    /**
     * Mount the query bool filter using an array
     *
     * @param [array] $query
     * @param [array] $values
     * @return array
     */
    public static function mountTermsPart($values)
    {
        $filter = [];
        if ($values) {
            foreach ($values as $key => $value) {
                if ($value) {
                    $filter[] = self::mountSingleTerm($key, $value);
                }
            }
        }
        return $filter;
    }

    /**
     * Mount each term/terms part
     *
     * @param [string] $key
     * @param [any] $value
     * @return array
     */
    private static function mountSingleTerm($key, $value)
    {
        if ($key == "_id" || $key == "id") {
            if (!is_array($value)) {
                $value = [$value];
            }
            $filter = [
                "ids" => ["values" => $value],
            ];
        } else {
            if (is_array($value)) {
                $search = "terms";
            } else {
                $search = "term";
            }
            $filter = [
                $search => [
                    $key => $value,
                ]
            ];
        }
        return $filter;
    }

    /**
     * mountAnArrayOfPrefixQuerys
     *
     * @param [array] $fields
     * @return array
     */
    public static function mountPrefix($fields)
    {
        $array = [];
        if (!is_array($fields)) {
            throw new Exception("Is not an array. The array need to has as the Key the field, and in the value the value to search");
        }
        foreach ($fields as $key => $value) {
            $array[] = [
                "prefix" => [
                    $key => $value
                ]
            ];
        }
        return $array;
    }

    //////////////////
    //
    // MOUNT ORDER
    //
    //////////////////

    /**
     * Mount the sort part of the query.
     * Like the SQL querys. If you want to order desc, add - to the field
     *
     * @param [array] $query
     * @param [array] $orders
     * @return array
     */
    public static function mountOrder($query, $orders)
    {
        $query["sort"] = self::getOrder($orders);
        return $query;
    }

    /**
     * Mount and array to use as order part of a query
     *
     * @param [array] $orders
     * @return array
     */
    public static function getOrder($orders)
    {
        $order = [];
        if (is_array($orders)) {
            $orders = [$orders];
        }
        $order = array_map(function ($order) {
            list($key, $di) = self::getOrderSingle($order);
            return [$key => ["order" => $di]];
        }, $orders);
    }

    /**
     * 
     * @param type $order
     * @return type
     */
    private static function getOrderSingle($order)
    {
        if (strpos($order, "-") === false) {
            $orderDi = self::ORDPOSITIVE;
        } else {
            $orderDi = self::ORDNEGATIVE;
            $order = str_replace("-", "", $order);
        }
        return [$order, $orderDi];
    }

    //////////////////
    //
    // MOUNT PAGINATION
    //
    //////////////////

    /**
     * If you pass a number, it will multiple the size/page
     * If you pass an array it will use it like search_after
     *
     * @param [int|array] $page
     * @return void
     */
    public static function mountPagination($query, $page)
    {
        if ($page) {
            if (is_numeric($page)) {
                $query['from'] = $query['size'] * $page;
            } else {
                $query['search_after'] = $page;
            }
        }
        return $query;
    }
}
